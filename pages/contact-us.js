import React, { useState } from "react";
import Link from "next/link";

function ContactUs() {
  return (
    <>
      <section className="banner_inner contact_banner">
        <div className="banner_content_inner text-center">
          <h2>Get In Touch</h2>
        </div>
      </section>
      <section className="about_section contact_section common_padding">
        <div className="container">
          <div className="row">
            <div className="col-md-6 col-sm-12 contact_section_left">
              <div className="row">
                <div className="col-md-6 col-sm-12">
                  <div className="contact_column_wrapper w-100 text-center">
                    <span>
                      <img src="/call.svg" alt="icon" />
                    </span>
                    <h3>Call Us</h3>
                    <p>
                      <a href="tel:9882340444">(+91) 9882340444</a>,{" "}
                      <a href="tel:8988312345">8988312345</a>
                    </p>
                  </div>
                </div>
                <div className="col-md-6 col-sm-12">
                  <div className="contact_column_wrapper w-100 text-center">
                    <span>
                      <img src="/mail.svg" alt="icon" />
                    </span>
                    <h3>Email Us</h3>
                    <p>
                      <a href="mailto:quartz@hotelhimalayanbrothers.com">
                        quartz@hotelhimalayanbrothers.com
                      </a>
                    </p>
                  </div>
                </div>
                <div className="col-md-6 col-sm-12">
                  <div className="contact_column_wrapper w-100 text-center">
                    <span>
                      <img src="/location.svg" alt="icon" />
                    </span>
                    <h3>Location</h3>
                    <p>
                      <a href="tel:9882340444">
                        Near Norbulingka, Dharamshala 176057, Himachal Pradesh,
                        India.
                      </a>
                    </p>
                  </div>
                </div>
                <div className="col-md-6 col-sm-12">
                  <div className="contact_column_wrapper w-100 text-center">
                    <span>
                      <img src="/time.svg" alt="icon" />
                    </span>
                    <h3>Opening Hours</h3>
                    <p>
                      <a>6 Am to 11 Pm</a>
                    </p>
                  </div>
                </div>
              </div>

              <div className="contact_social w-100">
                <label className="mb-0">Socials:</label>
                <a href="">
                  <img src="/fb-white.svg" alt="icon" />
                </a>
                <a href="">
                  <img src="/insta-white.svg" alt="icon" />
                </a>
                <a href="">
                  <img src="/twitter-white.svg" alt="icon" />
                </a>
              </div>
            </div>

            <div className="col-md-6 col-sm-12 contact_section_left">
              <form className="w-100">
                <div className="form-group ">
                  <label>Your Name</label>
                  <input type="text" name="" className="form-control" />
                </div>
                <div className="form-group ">
                  <label>Your Email</label>
                  <input type="text" name="" className="form-control" />
                </div>
                <div className="form-group ">
                  <label>Subject</label>
                  <input type="text" name="" className="form-control" />
                </div>
                <div className="form-group ">
                  <label>Message</label>
                  <textarea className="form-control"></textarea>
                </div>
                <div className="form-group mb-0 text-center">
                  <button type="submit" className="common_btn">
                    SUBMIT<i className="bi bi-arrow-right-short"></i>
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </section>

      <section className="contact_map w-100">
        <img src="/single-hotel/map-icon.jpg" alt="Image" />
      </section>
    </>
  );
}

export default ContactUs;
